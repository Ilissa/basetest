/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedonne;

/**
 *
 * @author lili
 */
public class Personne {
    private int id;
    private String nom;
    private String prenom;
    private int age;
    private String genre;
    //private static int Key=1;

    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    } 

    public Personne(int id, String nom, String prenom, int age, String genre) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.genre = genre;
    }
    

    public Personne(String nom, String prenom, int age, String genre) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.genre = genre;
       // this.id=Key;
        //Key++;
    }
    public Personne(){
        
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public int getAge() {
        return age;
    }

    public String getGenre() {
        return genre;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "Personne{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + 
                ", age=" + age + ", genre=" + genre + '}';
    }
    
    
    
}
