/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedonne;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author lili
 */
public interface DAO<T> {
  public void setPersonneDAO(Connection connexion)throws SQLException;
 public ArrayList<Personne>findAll()throws SQLException;
 public void insert(Personne personne)throws SQLException;
 public Personne findID(int id)throws SQLException;
 public void clear()throws SQLException;
  public ArrayList<Personne> personnebetween(int age1, int age2)throws SQLException;
    
}
