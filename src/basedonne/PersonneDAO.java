/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedonne;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author lili
 */
public class PersonneDAO implements DAO {
    private Connection connexion;

    @Override
    public void setPersonneDAO(Connection connexion)throws SQLException {
        this.connexion = connexion;
        connexion.setAutoCommit(false);
    }
    @Override
    public ArrayList<Personne>findAll()throws SQLException{
        ArrayList<Personne>list=new ArrayList<>();
    
    try(Statement stmt = connexion.createStatement();){
        ResultSet rset=stmt.executeQuery("SELECT * FROM PERSONNE ");
        while(rset.next()){
            Personne personne=new Personne();
            personne.setId(rset.getInt("id"));
            personne.setNom(rset.getString("nom"));
            personne.setPrenom(rset.getString("prenom"));
            personne.setAge(rset.getInt("age"));
            personne.setGenre(rset.getString("genre"));
            
         list.add(personne);
        }
     //  connexion.commit();
       
    
}
    return list;
    }
    
    @Override
    public void insert(Personne personne)throws SQLException{
       try( Statement stmt=connexion.createStatement()) {
        int rset=stmt.executeUpdate("INSERT INTO PERSONNE ( nom, prenom, age, genre)" 
                + " VALUES( \'" +personne.getNom()+"\',\'"+personne.getPrenom()+"\',"+
                personne.getAge()+",\'"+personne.getGenre()+"\')");
        
          connexion.commit();
       }catch(SQLException e){
           System.out.println(e);
           connexion.rollback();
        }  
    }
    @Override
    public Personne findID(int id)throws SQLException{
        Personne personne=null;
        try(Statement stmt = connexion.createStatement();){
             ResultSet rset=stmt.executeQuery("SELECT * FROM PERSONNE WHERE ID="+id);
            while(rset.next()){
                personne=new Personne();
                personne.setId(rset.getInt("id"));
                personne.setNom(rset.getString("nom"));
                personne.setPrenom(rset.getString("prenom"));
                personne.setGenre(rset.getString("genre"));
                personne.setAge(rset.getInt("age"));
                
                 
             }
            connexion.commit();
            
        }catch(SQLException e){
            connexion.rollback();
        }
        return personne;
    }
    @Override
    public ArrayList<Personne> personnebetween(int age1, int age2)throws SQLException{
        ArrayList<Personne>list=new ArrayList<>();
        try(Statement stmt = connexion.createStatement();){
            ResultSet rset=stmt.executeQuery("SELECT * FROM PERSONNE WHERE AGE BETWEEN "+age1+ " AND "+
                    age2);
             while(rset.next()){
            Personne personne=new Personne();
            personne.setId(rset.getInt("id"));
            personne.setNom(rset.getString("nom"));
            personne.setPrenom(rset.getString("prenom"));
            personne.setAge(rset.getInt("age"));
            personne.setGenre(rset.getString("genre"));
            
         list.add(personne);
        }
           connexion.commit();
        }catch(SQLException e){
            System.out.println(e);
            connexion.rollback();
        }
         return list;
    }
    @Override
    public void clear()throws SQLException{
       try(Statement stmt = connexion.createStatement();){ 
          stmt.execute("DELETE FROM PERSONNE");
          connexion.commit();
       }catch(SQLException e){
           e.getMessage();
       }  
    }
    
    
    
    public void supprimePersonne(Personne personne)throws SQLException{
     try(Statement stmt = connexion.createStatement();){ 
       int rset=stmt.executeUpdate("DELETE FROM PERSONNE WHERE ID=" +personne.getId());
        connexion.commit();
       }catch(SQLException e){
            connexion.rollback();
        }  
     
      
        
    }
    /*
    public void supprimeAll(ArrayList<Personne>list)throws SQLException{
      // try(Statement stmt = connexion.createStatement();){
           for(int i=0; i<list.size();i++){
               this.supprimePersonne(list.get(i));
           }
       }
*/
        
    }
    
    
    


   
    
    

